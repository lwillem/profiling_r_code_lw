#############################################################################
# PROFILING SCRIPT FOR R CODE WITH TIPS&TRICKS
#
# Find performance bottlenecks in your code using a default utility: rprof.
# The summary of the profiling tool will present the code-lines that are
# consuming the most runtime. Often, small changes can reduce the runtime
# significantly!
#
# Recommendations:
# - initiate the complete result-vector before the loop starts
# - replace for-loops (or while-loops) by vector-operations
# - use conditional-indexing instead of a for-loop with an if-else-clause
#
# Questions? => lander.willem@uantwerp.be
#
# Good Luck!
#
# Copyright 2016, Willem Lander, University of Antwerp.
#############################################################################
#
# This file is part of the indismo software. Indismo is an Open Source 
# project to model close-contact infectious disease transmission.
#
# More info on www.simid.be
#
#############################################################################
